/*
 * IncFile1.h
 *
 * Created: 5/12/2011 6:18:42 AM
 *  Author: Administrator
 */ 


#ifndef MAIN_H_
#define MAIN_H_

/* Include Statements */

#include "compiler.h"
#include "avr/interrupt.h"
#include "avr/io.h"
#include "avr/sleep.h"
#include "avr/wdt.h"
#include "avr/eeprom.h"

#include "common_constants.h"
#include "Initialization/custom_functions.h"


/* Variable Declarations */

bool			FIRSTPOWERUP		= 1;
bool			POWER_ON			= 0;
bool			POWER_OFF			= 0;
bool			PULSE_INPUT_ISR		= 0;
bool			TIMER_ISR			= 0;
bool			TIMEOUT_ISR			= 0;
bool			RF_RX_START_ISR		= 0;
bool			RF_RX_END_ISR		= 0;
bool			RF_TX_END_ISR		= 0;
bool			RF_WAKEUP			= 0;
bool			RF_AMI_ISR			= 0;
bool			COMP_H_UPDATE		= 0;
bool			COMP_L_UPDATE		= 0;
bool			PROBE_CONFIG_UPDATE	= 0;
bool            CHANNEL_SW_ACK      = 0;
bool            BATT_LOW			= 0;

unsigned char	COUNT_PINS			= 0x00;
unsigned char   COUNT_LOWBATT		= 0x00;

unsigned char	SOURCE				= 0x00;
unsigned char	AMP_GAIN			= 0x00;
unsigned char	COMP_H_THRESHOLD	= 0x00;
unsigned char	COMP_L_THRESHOLD	= 0x00;

/***************START CUT & PASTE**************/
//99Tc
unsigned char	AMP_GAIN_CAL1			 =0xC0;
unsigned char	COMP_H_THRESHOLD_CAL1	 =0x79;
unsigned char	COMP_L_THRESHOLD_CAL1	 =0x5d;

//111In
unsigned char	AMP_GAIN_CAL2			 =0xC0;
unsigned char	COMP_H_THRESHOLD_CAL2	 =0xC9;
unsigned char	COMP_L_THRESHOLD_CAL2	 =0x6F;

//511KeV
unsigned char	AMP_GAIN_CAL3			 =0x40;
unsigned char	COMP_H_THRESHOLD_CAL3	 =0x65;
unsigned char	COMP_L_THRESHOLD_CAL3	 =0x62;

//125I
unsigned char	AMP_GAIN_CAL4			 =0x90;
unsigned char	COMP_H_THRESHOLD_CAL4	 =0x42;
unsigned char	COMP_L_THRESHOLD_CAL4	 =0x3b;

































































/***************END CUT & PASTE****************/

unsigned int	MSG_TYPE_ERROR		= 0x0000;
unsigned int	PULSE_COUNT			= 0x0000;
unsigned int	BITBANG_COUNT_UPPER	= 0x0000;
unsigned int	BITBANG_COUNT_LOWER	= 0x0000;

bool			RFRX_ACK			= 1;
bool			RF_CHANNEL_SCAN		= 0;
bool            RF_ACK_REQ			= 0;
unsigned char	RF_CHANNEL			= 15;
unsigned char	RFRX_ERROR			= 0;
unsigned char	RFRX_ERROR_SCAN		= 10;
unsigned char	RFRX_ERROR_MAX		= 200;
unsigned char   RFTX_ACK_COUNT	    = 0;
unsigned char   RFRX_ACK_COUNT      = 0;

unsigned int	TEMP_TX_RATE		= 0;

unsigned char	MSG_TYPE			= 0;
unsigned char	MSG_DATA1			= 0;
unsigned char	MSG_DATA2			= 0;
bool			RF_ERROR			= 0;
unsigned int	RF_MSG_TYPE_ERROR	= 0;
unsigned int	RF_MSG_DATA1_ERROR	= 0;
unsigned int	RF_MSG_DATA2_ERROR	= 0;



///////////////////////////////
unsigned int	testtimercount		= 0;
unsigned int	testrfrxcount		= 0;
unsigned int	testrfrxerror		= 0;
unsigned int	testpoweron			= 0;
unsigned int	testpoweroff		= 0;
unsigned int	testpulsecount		= 0;
unsigned int	testpulsecountinc	= 0;
unsigned int	testpulsecountdec	= 0;
unsigned int	testpulse[255]		= {0};
////////////////////////////////

bool			PROBE_TEST				= 0;
unsigned char	PROBE_TEST_MODE			= 0;
unsigned char	PROBE_TEST_CMD			= 0;

unsigned char	LED_BLINK_RATE			= 0;
unsigned char	LED_BLINK_RATE_MAX		= 50;

/* Function Prototypes */

void rf_rx(void);
void pulse_transfer(void);

#endif /* MAIN_H_ */