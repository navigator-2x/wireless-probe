/*
 * IncFile1.h
 *
 * Created: 5/5/2011 9:22:26 PM
 *  Author: Administrator
 */ 


#ifndef INIT_GPIO_H_
#define INIT_GPIO_H_


void init_gpio(void);

void analog_pwr_enable(unsigned char ENABLE);

void count_int_enable(unsigned char ENABLE);
void accel_int1_int_enable(unsigned char ENABLE);
void accel_int2_int_enable(unsigned char ENABLE);

void led_toggle(void);
void led_on(void);
void led_off(void);

void init_accel_spi(unsigned char ENABLE);
void accel_spi_tx(unsigned char cData);

void init_dpot_spi(unsigned char ENABLE);
void dpot_set(unsigned char DPOT_SEL, unsigned char DPOT_CHANNEL, unsigned char DPOT_POSITION);

void init_timer1(unsigned int TX_RATE);
void timer1_enable(unsigned char ENABLE);

void init_rf(unsigned char ENABLE);
void rf_tx(unsigned char MSG_TYPE, unsigned char MSG_DATA1, unsigned char MSG_DATA2);
void set_batmon_threshold(void);

#endif /* INIT_GPIO_H_ */