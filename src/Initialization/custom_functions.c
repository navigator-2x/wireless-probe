#include "avr/io.h"
#include "avr/interrupt.h"

#include "custom_functions.h"
#include "common_constants.h"
#include "lis3dh_driver.h"

u8_t	ACCEL_WHO_AM_I		= 0;

u8_t	ACCEL_CTRL_REG1		= 0;
u8_t	ACCEL_CTRL_REG2		= 0;
u8_t	ACCEL_CTRL_REG3		= 0;
u8_t	ACCEL_CTRL_REG4		= 0;
u8_t	ACCEL_CTRL_REG5		= 0;
u8_t	ACCEL_CTRL_REG6		= 0;

u8_t	ACCEL_STATUS_REG	= 0;
u8_t	ACCEL_INT1_CFG		= 0;
u8_t	ACCEL_INT1_SRC		= 0;
u8_t	ACCEL_INT1_THS		= 0;
u8_t	ACCEL_INT1_DURATION	= 0;

u8_t	ACCEL_CLICK_CFG		= 0;
u8_t	ACCEL_CLICK_SRC		= 0;
u8_t	ACCEL_CLICK_THS		= 0;
u8_t	ACCEL_TIME_LIMIT	= 0;
u8_t	ACCEL_TIME_LATENCY	= 0;
u8_t	ACCEL_TIME_WINDOW	= 0;






/*
 * CProgram1.c
 *
 * Created: 5/5/2011 9:22:22 PM
 *  Author: Administrator
 */ 


/* GPIO Initialization Routine
 * Port B, Port D, Port E, Port F, and Port G only.
 * Port A and Port C are NOT used in ATmega128RFA1 Device.
 */

void init_gpio(void)
{
	MCUCR	&=	~(1 << PUD);							// Enable internal pull-up resistors, globally
	
	DPDS0	=	0x00;									// Port F, E, D, and B drive strength all set = 2mA
	DPDS1	=	0x00;									// Port G drive strength set = 2mA
	
	DDRB	=	0x07;									// Port B configured as output on pins 0-7
	DDRD	=	0xFF;									// Port D configured as output on pins 0-7
	DDRE	=	0xCE;									// Port E configured as output on pins 0-7
	DDRF	=	0xFF;									// Port F configured as output on pins 0-7
	DDRG	=	0xFF;									// Port G configured as output on pins 0-7
	
	PORTB	=	0x00;									// Port B configured as logic low or pull-up resistor off on pins 0-7
	PORTD	=	0x00;									// Port D configured as logic low or pull-up resistor off on pins 0-7
	PORTE	=	0x00;									// Port E configured as logic low or pull-up resistor off on pins 0-7
	PORTF	=	0x00;									// Port F configured as logic low or pull-up resistor off on pins 0-7
	PORTG	=	0x00;									// Port G configured as logic low or pull-up resistor off on pins 0-7
	
	PORTB	|=	(1 << PORTB2) | (1 << PORTB1) | (1 << PORTB0);					// Set CS to logic high
	//PORTD	|=	(1 << PORTD7);							// Enable power to analog circuit
	PORTD	|=	(1 << PORTD0);							// Set DPOT_CS1 line
	PORTD	|=	(1 << PORTD1);							// Set DPOT_CS2 line
	PORTE	|=	(1 << PORTE4) | (1 << PORTE5);			// COUNT_UP & COUNT_DOWN pull-up resistor enabled
	PORTF	|=	(1 << PORTF0) | (1 << PORTF1);			// MCU_LED_OUT1 & MCU_LED_OUT2 output high to disable LEDs
	
	//EICRB	=	0x00;									// Configure INT7-4 on Port E (GPIO_1 & GPIO_2) for low level trigger
	EICRB	|=	(1 << ISC51) | (1 << ISC41);			// Configure INT5-4 on Port E (GPIO_1 & GPIO_2) for falling edge trigger
	//EIMSK	|=	(1 << INT5) | (1 << INT4);				// Enable COUNT_UP & COUNT_DOWN interrupt
	
	PCICR	|=	(1 << PCIE0);							// Enable PCINT7:0 globally
	
	EIFR	=	0xFF;									// Clear all external interrupt flags
	PCIFR	=	0xFF;									// Clear all pin change interrupt flags
}


void analog_pwr_enable(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
		PORTD	&=	~(1 << PORTD7);							// Disable power to analog circuit
			break;
		case 1:
		PORTD	|=	(1 << PORTD7);							// Enable power to analog circuit
			break;
		default:
			break;
	}
}


void count_int_enable(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
		EIMSK	&=	~(1 << INT5)	& ~(1 << INT4);			// Disable COUNT_UP & COUNT_DOWN interrupt
			break;
		case 1:
		EIMSK	|=	(1 << INT5)		| (1 << INT4);			// Enable COUNT_UP & COUNT_DOWN interrupt
			break;
		default:
			break;
	}
	
	EIFR	=	0xFF;										// Clear all external interrupt flags
}


void accel_int1_int_enable(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
		PCMSK0	&=	~(1 << PCINT4);							// Disable ACCEL_INT1 input interrupt
			break;
		case 1:
		PCMSK0	|=	(1 << PCINT4);							// Enable ACCEL_INT1 input interrupt
			break;
		default:
			break;
	}
	
	PCIFR	|=	0xFF;										// Clear all Pin Change interrupt flags
}


void accel_int2_int_enable(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
		PCMSK0	&=	~(1 << PCINT5);							// Disable ACCEL_INT2 input interrupt
			break;
		case 1:
		PCMSK0	|=	(1 << PCINT5);							// Enable ACCEL_INT2 input interrupt
			break;
		default:
			break;
	}
	
	PCIFR	|=	0xFF;										// Clear all Pin Change interrupt flags
}


void led_toggle(void)
{
	PORTF	^=	(1 << PORTF0);								// MCU_LED_OUT1 'toggle'
	PORTF	^=	(1 << PORTF1);								// MCU_LED_OUT2 'toggle'
}


void led_on(void)
{
	PORTF	&=	~(1 << PORTF0);								// MCU_LED_OUT1 'on'
	PORTF	&=	~(1 << PORTF1);								// MCU_LED_OUT2 'on'
}


void led_off(void)
{
	PORTF	|=	(1 << PORTF0);								// MCU_LED_OUT1 'off'
	PORTF	|=	(1 << PORTF1);								// MCU_LED_OUT2 'off'
}


void init_accel_spi(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
		SPCR		&=	~(1 << SPE);						// Disable SPI
			break;
		case 1:
		SPCR		|=	(1 << SPE) | (1 << MSTR) | (1 << CPOL) | (1 << CPHA)  | (1 << SPR1) | (1 << SPR0);		// Enable SPI, Master, CLK idle high, sample rising edge, set clock rate fck/128
		
		GetWHO_AM_I(&ACCEL_WHO_AM_I);
		
		// Configure CTRL_REG1
		SetODR(ODR_100Hz);
		SetMode(LOW_POWER);
		SetAxis(X_ENABLE|Y_ENABLE|Z_ENABLE);
		
		// Configure CTRL_REG2
		// Default configuration
		
		// Configure CTRL_REG3
		SetInt1Pin( CLICK_ON_PIN_INT1_ENABLE	|	I1_INT1_ON_PIN_INT1_DISABLE	|
					I1_INT2_ON_PIN_INT1_DISABLE	|	I1_DRDY1_ON_INT1_DISABLE	|
					I1_DRDY2_ON_INT1_DISABLE	|	WTM_ON_INT1_DISABLE			|
					INT1_OVERRUN_DISABLE);
				
		// Configure CTRL_REG4
		SetFullScale(FULLSCALE_4);
		
		// Configure CTRL_REG5
		// Default configuration
		
		// Configure CTRL_REG6
		SetInt2Pin(	CLICK_ON_PIN_INT2_DISABLE	|	I2_INT1_ON_PIN_INT2_DISABLE	|
					I2_INT2_ON_PIN_INT2_DISABLE	|	I2_BOOT_ON_INT2_DISABLE		|
					INT_ACTIVE_HIGH);
		
		// Configure Reference
		// Default configuration
		
		// Configure Click Registers
		SetClickTHS(20);
		SetClickLIMIT(20);
		SetClickLATENCY(10);
		SetClickWINDOW(100);
		SetClickCFG(	ZD_ENABLE	|	ZS_DISABLE	|	YD_ENABLE	| 
						YS_DISABLE	|	XD_ENABLE	|	XS_DISABLE	);
		
		// Read registers from accelerometer //
		ReadReg(CTRL_REG1,		&ACCEL_CTRL_REG1);
		ReadReg(CTRL_REG2,		&ACCEL_CTRL_REG2);
		ReadReg(CTRL_REG3,		&ACCEL_CTRL_REG3);
		ReadReg(CTRL_REG4,		&ACCEL_CTRL_REG4);
		ReadReg(CTRL_REG5,		&ACCEL_CTRL_REG5);
		ReadReg(CTRL_REG6,		&ACCEL_CTRL_REG6);
		
		ReadReg(STATUS_REG,		&ACCEL_STATUS_REG);
		ReadReg(INT1_CFG,		&ACCEL_INT1_CFG);
		ReadReg(INT1_SRC,		&ACCEL_INT1_SRC);
		ReadReg(INT1_THS,		&ACCEL_INT1_THS);
		ReadReg(INT1_DURATION,	&ACCEL_INT1_DURATION);
		
		ReadReg(CLICK_CFG,		&ACCEL_CLICK_CFG);
		ReadReg(CLICK_SRC,		&ACCEL_CLICK_SRC);
		ReadReg(CLICK_THS,		&ACCEL_CLICK_THS);
		ReadReg(TIME_LIMIT,		&ACCEL_TIME_LIMIT);
		ReadReg(TIME_LATENCY,	&ACCEL_TIME_LATENCY);
		ReadReg(TIME_WINDOW,	&ACCEL_TIME_WINDOW);

			break;
		default:
			break;
	}
}


void accel_spi_tx(unsigned char cData)
{
	SPDR			=	cData;							// Start transmission
	while( !(SPSR & (1 << SPIF) ) );					// Wait for transmission complete
}


void init_dpot_spi(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
		UCSR1B		=	0;																					// Disable SPI
		//PORTD		&=	~(1 << PORTD0);																		// Clear DPOT_CS1 line
		//PORTD		&=	~(1 << PORTD1);																		// Clear DPOT_CS2 line
			break;
		case 1:
		UBRR1		=	0;																					// Set Baud Rate
		DDRD		|=	(1 << PORTD1);																		// Set clock pin to output
		UCSR1C		=	(1 << UMSEL11) | (1 << UMSEL10) | (0 << UDORD1) | (1 << UCPHA1) | (1 << UCPOL1);	// Set MSPI mode of operation and SPI data mode 0 (Msb first, clock idle low, sample on rising edge)
		UCSR1B		=	(1 << TXEN1);																		// Enable transmitter
		UBRR1		=	1;																					// Set Baud Rate (Must be set after the transmitter is enabled
		PORTD		|=	(1 << PORTD0);																		// Set DPOT_CS1 line
		PORTD		|=	(1 << PORTD1);																		// Set DPOT_CS2 line
			break;
		default:
			break;
	}
}


void dpot_set(unsigned char DPOT_SEL, unsigned char DPOT_CHANNEL, unsigned char DPOT_POSITION)
{
	/* CLEAR CS LINE FOR DPOT1 OR DPOT2 */
	switch (DPOT_SEL)
	{
		case 0: 
		PORTD			&=	~(1 << PORTD0);				// Clear DPOT_CS1 line
			break;
		case 1: 
		PORTD			&=	~(1 << PORTD1);				// Clear DPOT_CS2 line
			break;
		default:
			break;
	}
	
	
	for (unsigned int i = 0; i < 1000; i++);					// 250us Delay
	
	
	/* TRANSMIT ADDRESS FOR REGISTER A OR B AND THEN TRANSMIT WIPER POSITION */
	UDR1 = DPOT_CHANNEL;										// Start transmission
	while ( !( UCSR1A & (1<<UDRE1)) );							// Wait for transmission complete
	
	UDR1 = DPOT_POSITION;										// Start transmission
	while ( !( UCSR1A & (1<<UDRE1)) );							// Wait for transmission complete
	
	for (unsigned int i = 0; i < 1000; i++);					// 250us Delay
	
	/* SET CS LINE FOR DPOT1 OR DPOT2 */
	switch (DPOT_SEL)
	{
		case 0: 
		PORTD			|=	(1 << PORTD0);						// Set DPOT_CS1 line
			break;
		case 1: 
		PORTD			|=	(1 << PORTD1);						// Set DPOT_CS2 line
			break;
		default:
			break;
	}
}


void init_timer1(unsigned int TX_RATE)
{
	// Timer 1 Configuration
	GTCCR	|=	(1 << TSM);								// Activate Timer synchronization mode
	TCCR1B	|=	(1 << CS11) | (1 << CS10) | (1 << WGM12);		// Timer 1 clock select for divide by 64 and "CTC Mode"
	TCCR1A	=	0x00;									// Configure Timer 1 for normal port operation and "CTC Mode"
	
	//OCR1A	=	RF_RATE_10MS;							// Configure the output capture and compare register to rate in header file
	OCR1A	=	TX_RATE;								// Configure the output capture and compare register to rate in header file
	
	TCNT1	=	0x0000;									// Clear the Timer 1 count
	GTCCR	&=	~(1 << TSM);							// Exit Timer synchronization mode
}


void timer1_enable(unsigned char ENABLE)
{
	switch (ENABLE)
	{
		case 0:
		TCCR1B	=	0x00;											// Stop the Timer 1 clock
		TCNT1	=	0x0000;											// Clear the Timer 1 count
			break;
		case 1:
		TCCR1B	|=	(1 << CS11) | (1 << CS10) | (1 << WGM12);		// Timer 1 clock select for divide by 64 and "CTC Mode"
		TIMSK1	|=	(1 << TOIE1) | (1 << OCIE1A);					// Enable the Timer 1 overflow interrupt
			break;
		default:
			break;
	}
}


void init_rf(unsigned char ENABLE)
{
	TRX_CTRL_2	&=	~(1 << OQPSK_DATA_RATE1) & ~(1 << OQPSK_DATA_RATE0);				// Configure RF Transceiver for 250kbps Data Rate
	
	switch (ENABLE)
	{
		case 0:
		IRQ_MASK	&=	~(1 << RX_END_EN);												// Disable RF interrupts
		
		TRX_STATE	= TRX_OFF;															// RF Transceiver to TRX_OFF mode (XOSC=ON)
		for (unsigned int i = 0; i < 20; i++);											// 5us Delay (1us required for transition)
		
		TRXPR		|=	(1 << SLPTR);													// RF Transceiver TRX_OFF --> SLEEP
		for (unsigned int i = 0; i < 20; i++);											// 5us Delay (2us required for transition)
			break;
		case 1:
		TRXPR		&=	~(1 << SLPTR);													// RF Transceiver SLEEP --> TRX_OFF
		
		TRX_STATE	= TRX_OFF;															// RF Transceiver to TRX_OFF mode (XOSC=ON)
		for (unsigned int i = 0; i < 2000; i++);										// 500us Delay (240us required for transition)
		
		IRQ_MASK	|=	(1 << RX_END_EN);												// Enable RF interrupts
			break;
		default:
			break;
	}
	
	for (unsigned int i = 0; i <= 127; i++)
	{
		_SFR_MEM8(0x180 + i)=	0x00;
	}
}


void rf_tx(unsigned char MSG_TYPE, unsigned char MSG_DATA1, unsigned char MSG_DATA2)
{
	TRX_STATE	=	PLL_ON;						// RF Transceiver to RX_ON mode (Rx Listen State)
	for (unsigned int i = 0; i < 1000; i++);	// 250us Delay (110us required for transition)


	TRXFBST				=	0x09;
	_SFR_MEM8(0x181)	=	0x1D;
	//_SFR_MEM8(0x181)	=	MSG_TYPE;
	_SFR_MEM8(0x182)	=	0xDA;
	//_SFR_MEM8(0x182)	=	MSG_DATA1;
	_SFR_MEM8(0x183)	=	0xFF;
	//_SFR_MEM8(0x183)	=	MSG_DATA2;
	_SFR_MEM8(0x184)	=	MSG_TYPE;
	_SFR_MEM8(0x185)	=	MSG_DATA1;
	_SFR_MEM8(0x186)	=	MSG_DATA2;
	
	TRX_STATE	=	CMD_TX_START;				// RF Transceiver to BUSY_TX mode (Transmit State)
}

void set_batmon_threshold(void)
{
	 
	BATMON &= ~(1 << BATMON_VTH0);
	BATMON &= ~(1 << BATMON_VTH1);
	BATMON &= ~(1 << BATMON_VTH2);							 // Set battery detection threshold to 0x04 = 2.850 V 
	BATMON &= ~(1 << BATMON_VTH3);
	BATMON |= (1 << BATMON_HR);							// Set high battery voltage threshold range
	//BATMON |= (1 << BAT_LOW_EN);							// Enable low battery interrupt	
}