/*
 * CProgram1.c
 *
 * Created: 5/6/2011 1:07:15 AM
 *  Author: Administrator
 * Modified R. Velarde 12/2014 to add battery level detection
 */ 

#include "main.h"

/*
 *	INTERRUPT SERVICE ROUTINES
 */

ISR(PCINT0_vect)			// Accelerometer Motion Interrupt (Service in main loop)
{
	POWER_ON		=	1;
}

ISR(INT5_vect)				// COUNT_DOWN Pulse Input
{
	if(PULSE_COUNT != 0)	// Prevent decrement of Pulse Counter, if already 0
	{
		PULSE_COUNT--;		// Decrement the Pulse Counter
	}
}

ISR(INT4_vect)				// COUNT_UP Pulse Input
{
	PULSE_COUNT++;			// Increment the Pulse Counter
}

ISR(TIMER1_COMPA_vect)		// Pulse Counter Reset and RF Transmit Timer (Service in main loop)
{
	TIMER_ISR			=	1;
}

ISR(TRX24_RX_END_vect)		// RF Transmit is Complete (Service in main loop)
{
	RF_RX_END_ISR		=	1;
}

ISR(BAT_LOW_vect)			// Low battery condition detection
{
	//BATT_LOW			=   1;
	BATMON |= (1 << BAT_LOW);	// Clear flag
}


/*
 *	RF RECEIVE FUNCTION
 */

void rf_rx(void)
{
	RF_ERROR	= 0;								// Clear RF_ERROR flag
	
	if ((_SFR_MEM8(0x180) == 0x1D) && (_SFR_MEM8(0x181) == 0xDA))						// Data Header Check
	{
		if(_SFR_MEM8(0x182) == 0x00)								// Verify is a dongle-to-probe message
		{
			MSG_TYPE	= _SFR_MEM8(0x183);								// read value
			MSG_DATA1	= _SFR_MEM8(0x184);								//  read value
			MSG_DATA2	= _SFR_MEM8(0x185);								//  read value
		}
		else
		{
			RF_ERROR	= 1;	
		}
	}
	else															// If Redundant Characters do NOT match...
	{
		RF_MSG_TYPE_ERROR++;										// Increment error counter
		RF_MSG_DATA1_ERROR++;										// Increment error counter
		RF_MSG_DATA2_ERROR++;										// Increment error counter
		
		RF_ERROR	= 1;											// Set RF_ERROR flag
	}
	
	if (RF_ERROR)									// If a bit error was detected, ignore the message data
	{
		// Do Nothing
	}
	else
	{
		switch (MSG_TYPE)							// If ZERO bit errors were detected, perform the action, based on the MSG_TYPE
		{
			case MSG_ACK:							// ACK Message
				RF_ACK_REQ = 0;					// Clear acknowledgment request flag
				break;
			
			case MSG_CAL_SETTINGS:					// Restore Calibration Settings Message
				SOURCE = MSG_DATA1;

				switch (SOURCE)						// Set Gain and Threshold variables, based on Source selected
				{
					case TC99:
						AMP_GAIN = AMP_GAIN_CAL1;
						COMP_H_THRESHOLD = COMP_H_THRESHOLD_CAL1;
						COMP_L_THRESHOLD = COMP_L_THRESHOLD_CAL1;
						break;
				
					case IN111:
						AMP_GAIN = AMP_GAIN_CAL2;
						COMP_H_THRESHOLD = COMP_H_THRESHOLD_CAL2;
						COMP_L_THRESHOLD = COMP_L_THRESHOLD_CAL2;
						break;
				
					case I131:
						AMP_GAIN = AMP_GAIN_CAL3;
						COMP_H_THRESHOLD = COMP_H_THRESHOLD_CAL3;
						COMP_L_THRESHOLD = COMP_L_THRESHOLD_CAL3;
						break;
				
					case I125:
						AMP_GAIN = AMP_GAIN_CAL4;
						COMP_H_THRESHOLD = COMP_H_THRESHOLD_CAL4;
						COMP_L_THRESHOLD = COMP_L_THRESHOLD_CAL4;
						break;
					
					default:
						break;
				}
			
				dpot_set(0, 0, AMP_GAIN);			// Program Gain
				dpot_set(1, 0, COMP_L_THRESHOLD);	// Program Lower Threshold
				dpot_set(1, 1, COMP_H_THRESHOLD);	// Program Upper Threshold
				
				timer1_enable(0);						// Disable Timer 1
				PULSE_COUNT	= 0;						// Clear Pulse Count
				timer1_enable(1);						// Enable Timer 1
				break;
		
			case MSG_THRESHOLD_SET:						// Probe Threshold Change Message
				COMP_L_THRESHOLD = MSG_DATA1;
				COMP_H_THRESHOLD = MSG_DATA2;
		
				dpot_set(1, 0, COMP_L_THRESHOLD);	// Program Lower Threshold
				dpot_set(1, 1, COMP_H_THRESHOLD);	// Program Upper Threshold
				break;
		
			case MSG_GAIN_SET:							// Probe Gain Change Message
				AMP_GAIN = MSG_DATA1;
		
				dpot_set(0, 0, AMP_GAIN);			// Program Gain
				break;
				
			case MSG_THRESHOLD_INC:						// Probe Threshold Change Message
				switch (MSG_DATA1)						// Set Gain and Threshold variables, based on Source selected
				{
					case 0:
						COMP_L_THRESHOLD	= COMP_L_THRESHOLD + MSG_DATA2;		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
						break;
						
					case 1:
						COMP_H_THRESHOLD	= COMP_H_THRESHOLD + MSG_DATA2;		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
						break;
						
					default:
						break;
				}
				
				dpot_set(1, 0, COMP_L_THRESHOLD);	// Program Lower Threshold
				dpot_set(1, 1, COMP_H_THRESHOLD);	// Program Upper Threshold
				break;
						
			case MSG_THRESHOLD_DEC:						// Probe Threshold Change Message
				switch (MSG_DATA1)						// Set Gain and Threshold variables, based on Source selected
				{
					case 0:
						COMP_L_THRESHOLD	= COMP_L_THRESHOLD - MSG_DATA2;		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
						break;
						
					case 1:
						COMP_H_THRESHOLD	= COMP_H_THRESHOLD - MSG_DATA2;		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
						break;
						
					default:
						break;
				}
				
				dpot_set(1, 0, COMP_L_THRESHOLD);	// Program Lower Threshold
				dpot_set(1, 1, COMP_H_THRESHOLD);	// Program Upper Threshold
				break;
		
			case MSG_GAIN_CHANGE:							// Probe Gain Change Message
				switch (MSG_DATA1)						// Set Gain and Threshold variables, based on Source selected
				{
					case 0:
						AMP_GAIN	= AMP_GAIN - MSG_DATA2;		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
						break;
						
					case 1:
						AMP_GAIN	= AMP_GAIN + MSG_DATA2;		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
						break;
						
					default:
						break;
				}
				
				dpot_set(0, 0, AMP_GAIN);			// Program Gain
				break;
				
			case MSG_CHANNEL_SET:						// RF Channel Change Message
				//PHY_CC_CCA			= MSG_DATA1;	    // Change RF Channel -Not immediate change;issue flag
				CHANNEL_SW_ACK      =  1;
				break;
				
			case MSG_CAL2FLASH:						// RF Channel Change Message
				SOURCE = MSG_DATA1;
								
				switch (SOURCE)						// Set Gain and Threshold variables, based on Source selected
				{
					case TC99:
						eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL1,COMP_H_THRESHOLD);		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
						eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL1,COMP_L_THRESHOLD);		// Commit COMP_L_THRESHOLD_CAL1 to EEPROM
						eeprom_write_byte(EEPROM_AMP_GAIN_CAL1,AMP_GAIN);						// Commit AMP_GAIN_CAL1 to EEPROM
						break;
									
					case IN111:
						eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL2,COMP_H_THRESHOLD);		// Commit COMP_H_THRESHOLD_CAL2 to EEPROM
						eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL2,COMP_L_THRESHOLD);		// Commit COMP_L_THRESHOLD_CAL2 to EEPROM
						eeprom_write_byte(EEPROM_AMP_GAIN_CAL2,AMP_GAIN);						// Commit AMP_GAIN_CAL2 to EEPROM
						break;
									
					case I131:
						eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL3,COMP_H_THRESHOLD);		// Commit COMP_H_THRESHOLD_CAL3 to EEPROM
						eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL3,COMP_L_THRESHOLD);		// Commit COMP_L_THRESHOLD_CAL3 to EEPROM
						eeprom_write_byte(EEPROM_AMP_GAIN_CAL3,AMP_GAIN);						// Commit AMP_GAIN_CAL3 to EEPROM
						break;
									
					case I125:
						eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL4,COMP_H_THRESHOLD);		// Commit COMP_H_THRESHOLD_CAL4 to EEPROM
						eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL4,COMP_L_THRESHOLD);		// Commit COMP_L_THRESHOLD_CAL4 to EEPROM
						eeprom_write_byte(EEPROM_AMP_GAIN_CAL4,AMP_GAIN);						// Commit AMP_GAIN_CAL4 to EEPROM
						break;
									
					default:
					break;
				}
				break;
				
			case MSG_TX_RATE_SET:								// RF Channel Change Message
				TEMP_TX_RATE	= MSG_DATA1;
				TEMP_TX_RATE	= ( ((TEMP_TX_RATE << 8) & 0xFF00) | MSG_DATA2 );
			
				init_timer1(TEMP_TX_RATE);						// Initialize Timer 1 for New Rate
				break;
				
			case MSG_TESTMODE:								// Enable Probe Test Mode Message
				switch (MSG_DATA1)
				{
					case TEST_RF_TX:						// Test Mode is RF_TX
						PHY_CC_CCA			= MSG_DATA2;	// Change RF Channel
						PROBE_TEST_MODE		= TEST_RF_TX;	// Set Test Mode to RF_TX
						PROBE_TEST			= 1;			// Set PROBE_TEST Flag
						break;
			
					case TEST_RF_RX:						// Test Mode is RF_RX
						PROBE_TEST_MODE		= TEST_RF_RX;	// Set Test Mode to RF_RX
						PROBE_TEST			= 1;			// Set PROBE_TEST Flag
						break;
			
					default:
						// Do Nothing
						break;
				}
				break;

			default:
				MSG_TYPE_ERROR++;					// MSG_TYPE was NOT Recognized
				break;
		}
	}
}


/*
 *	MAIN FUNCTION
 */

int main(void)
{
	MCUSR = 0;										//
	wdt_reset();									// Reset WDT
	wdt_disable();									// Disable WDT
	wdt_enable(WDTO_4S);							// Enable WDT for 4s Timeout
	
	switch (FIRSTPOWERUP)
	{
		case 0:
			// Do Nothing
			break;
		
		case 1:
			cli();									// Disable Interrupts
			wdt_reset();							// Reset the WDT
			
			init_gpio();							// Initialize GPIO
			init_timer1(RF_RATE_25MS);				// Initialize Timer 1 for 10ms Rate
			
			accel_int1_int_enable(1);				// Enable Accelerometer Interrupt 1
			init_accel_spi(1);						// Initialize Accelerometer
			
			TRXPR	|=	(1 << SLPTR);				// Configure RF transceiver into low power mode
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);	// Configure sleep mode for PWR_DOWN
			
			FIRSTPOWERUP	= 0;					// Clear the FIRSTPOWERUP flag
			
			if (eeprom_read_byte(EEPROM_EMPTY) == 0xFF)
			{
				eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL1,COMP_H_THRESHOLD_CAL1);		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
				eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL1,COMP_L_THRESHOLD_CAL1);		// Commit COMP_L_THRESHOLD_CAL1 to EEPROM
				eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL2,COMP_H_THRESHOLD_CAL2);		// Commit COMP_H_THRESHOLD_CAL2 to EEPROM
				eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL2,COMP_L_THRESHOLD_CAL2);		// Commit COMP_L_THRESHOLD_CAL2 to EEPROM
				eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL3,COMP_H_THRESHOLD_CAL3);		// Commit COMP_H_THRESHOLD_CAL3 to EEPROM
				eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL3,COMP_L_THRESHOLD_CAL3);		// Commit COMP_L_THRESHOLD_CAL3 to EEPROM
				eeprom_write_byte(EEPROM_COMP_H_THRESHOLD_CAL4,COMP_H_THRESHOLD_CAL4);		// Commit COMP_H_THRESHOLD_CAL4 to EEPROM
				eeprom_write_byte(EEPROM_COMP_L_THRESHOLD_CAL4,COMP_L_THRESHOLD_CAL4);		// Commit COMP_L_THRESHOLD_CAL4 to EEPROM
					
				eeprom_write_byte(EEPROM_AMP_GAIN_CAL1,AMP_GAIN_CAL1);						// Commit AMP_GAIN_CAL1 to EEPROM
				eeprom_write_byte(EEPROM_AMP_GAIN_CAL2,AMP_GAIN_CAL2);						// Commit AMP_GAIN_CAL2 to EEPROM
				eeprom_write_byte(EEPROM_AMP_GAIN_CAL3,AMP_GAIN_CAL3);						// Commit AMP_GAIN_CAL3 to EEPROM
				eeprom_write_byte(EEPROM_AMP_GAIN_CAL4,AMP_GAIN_CAL4);						// Commit AMP_GAIN_CAL4 to EEPROM
					
				eeprom_write_byte(EEPROM_EMPTY,0);											// Clear EEPROM_EMPTY byte in EEPROM
			}
			else
			{
				COMP_H_THRESHOLD_CAL1	=		eeprom_read_byte(EEPROM_COMP_H_THRESHOLD_CAL1);		// Commit COMP_H_THRESHOLD_CAL1 to EEPROM
				COMP_L_THRESHOLD_CAL1	=		eeprom_read_byte(EEPROM_COMP_L_THRESHOLD_CAL1);		// Commit COMP_L_THRESHOLD_CAL1 to EEPROM
				COMP_H_THRESHOLD_CAL2	=		eeprom_read_byte(EEPROM_COMP_H_THRESHOLD_CAL2);		// Commit COMP_H_THRESHOLD_CAL2 to EEPROM
				COMP_L_THRESHOLD_CAL2	=		eeprom_read_byte(EEPROM_COMP_L_THRESHOLD_CAL2);		// Commit COMP_L_THRESHOLD_CAL2 to EEPROM
				COMP_H_THRESHOLD_CAL3	=		eeprom_read_byte(EEPROM_COMP_H_THRESHOLD_CAL3);		// Commit COMP_H_THRESHOLD_CAL3 to EEPROM
				COMP_L_THRESHOLD_CAL3	=		eeprom_read_byte(EEPROM_COMP_L_THRESHOLD_CAL3);		// Commit COMP_L_THRESHOLD_CAL3 to EEPROM
				COMP_H_THRESHOLD_CAL4	=		eeprom_read_byte(EEPROM_COMP_H_THRESHOLD_CAL4);		// Commit COMP_H_THRESHOLD_CAL4 to EEPROM
				COMP_L_THRESHOLD_CAL4	=		eeprom_read_byte(EEPROM_COMP_L_THRESHOLD_CAL4);		// Commit COMP_L_THRESHOLD_CAL4 to EEPROM
					
				AMP_GAIN_CAL1			=		eeprom_read_byte(EEPROM_AMP_GAIN_CAL1);				// Commit AMP_GAIN_CAL1 to EEPROM
				AMP_GAIN_CAL2			=		eeprom_read_byte(EEPROM_AMP_GAIN_CAL2);				// Commit AMP_GAIN_CAL2 to EEPROM
				AMP_GAIN_CAL3			=		eeprom_read_byte(EEPROM_AMP_GAIN_CAL3);				// Commit AMP_GAIN_CAL3 to EEPROM
				AMP_GAIN_CAL4			=		eeprom_read_byte(EEPROM_AMP_GAIN_CAL4);				// Commit AMP_GAIN_CAL4 to EEPROM
			}
			
			sei();									// Enable Interrupts
			sleep_mode();							// Place MCU into sleep mode
			break;
			
		default:
			break;
	}

	while (1)
	{

		/////////////////////
		// Probe Test Loop //
		/////////////////////
		while (PROBE_TEST)		// When PROBE_TEST flag is set, enter this loop (CANNOT EXIT UNTIL RESET)
		{
			cli();				// Disable Interrupts
			wdt_disable();		// Disable WDT
			
			switch (PROBE_TEST_MODE)
			{
				case TEST_RF_TX:
					while (1)	// When Test Mode is RF_TX, enter this loop (CANNOT EXIT UNTIL RESET)
					{
						led_toggle();
						
						for (unsigned char newmsg = 0; newmsg < 255; newmsg++)
						{
							rf_tx(MSG_COUNT,newmsg,newmsg);						// Transmit incrementing message
							while ( !( IRQ_STATUS & (1 << TX_END)) );			// Wait for transmission complete
							IRQ_STATUS |=	(1 << TX_END);						// Clear TX_END IRQ
						};
					}
					break;
				
				case TEST_RF_RX:
					TRX_STATE	= RX_ON;							// RF Transceiver to RX_ON mode (Rx Listen State)
					for (unsigned int i = 0; i < 20; i++);			// 5us Delay (1us required for transition)
					break;
				
				default:
					break;
			}
		}

		///////////////////////////////////////////////////
		// Pulse Counter Reset and RF Transmit Timer ISR //
		///////////////////////////////////////////////////
		if (TIMER_ISR)
		{
			cli();													// Disable Interrupts
			wdt_reset();											// Disable WDT
			
			testtimercount++;///////////////////////////////////// TEST CODE!!!
			
			count_int_enable(0);									// Disable Pulse Count Interrupts

			//if(BATMON & (1 << BATMON_OK))
			//{
			//	BATT_LOW = false;
			//}
			//else
			//{
			//	BATT_LOW = true;
			//	led_on();
			//}

			if(!BATT_LOW)
			{
				LED_BLINK_RATE++;
			
				if (LED_BLINK_RATE > ( (LED_BLINK_RATE_MAX >> 1) & 0x7F) )
				{
					led_on();											// LED On
				}
				if (LED_BLINK_RATE > LED_BLINK_RATE_MAX)
				{
					LED_BLINK_RATE = 0;
				}
			}
			
			
			
			/*if (RF_CHANNEL_SCAN)
			{
				RF_CHANNEL++;
							
				if (RF_CHANNEL > 26)
				{
					RF_CHANNEL	= 11;
				}
							
				PHY_CC_CCA		= RF_CHANNEL;	// Change RF Channel
			}*/
			//PULSE_COUNT = 0;
			
			//if(PULSE_COUNT > 0)
			RFTX_ACK_COUNT++;
			if(PROBE_CONFIG_UPDATE)
			{
					rf_tx(MSG_ACK,0x0A,0);									// Transmit ACK Message with Source mode config request
					
					RF_ACK_REQ = 1;
					PROBE_CONFIG_UPDATE = 0;
					while ( !( IRQ_STATUS & (1 << TX_END)) );				// Wait for transmission complete
					IRQ_STATUS |=	(1 << TX_END);							// Clear TX_END IRQ
			}
			else if(CHANNEL_SW_ACK)
			{
					
					rf_tx(MSG_ACK,0x0C,0);									// Transmit ACK Message	
					RF_ACK_REQ = 1;
					while ( !( IRQ_STATUS & (1 << TX_END)) );				// Wait for transmission complete
					IRQ_STATUS |=	(1 << TX_END);							// Clear TX_END IRQ
					
					PHY_CC_CCA			= MSG_DATA1;						//Switch RF channel
					CHANNEL_SW_ACK = 0;
					
			}
			else if(RFTX_ACK_COUNT == 32)
			{
					rf_tx(MSG_ACK,0,0);									// Transmit ACK Message
					
					RF_ACK_REQ = 1;
					while ( !( IRQ_STATUS & (1 << TX_END)) );				// Wait for transmission complete
					IRQ_STATUS |=	(1 << TX_END);							// Clear TX_END IRQ
			}
			else
			{

				rf_tx(MSG_COUNT,(PULSE_COUNT & 0x00FF),((PULSE_COUNT >> 8) & 0x00FF));	// Transmit the Pulse Count to the Dongle
				
				RF_ACK_REQ = 1;
				RFRX_ACK_COUNT++;
				
				PULSE_COUNT	=	0;										// Pulse Counter Reset
			
				while ( !( IRQ_STATUS & (1 << TX_END)) );				// Wait for transmission complete
				IRQ_STATUS |=	(1 << TX_END);							// Clear TX_END IRQ
				

				//cli();													// Disable Interrupts
				//wdt_reset();
				if(!BATT_LOW)
				{
					if (LED_BLINK_RATE < ( (LED_BLINK_RATE_MAX >> 1) & 0x7F) )
					{
							led_off();									// LED Off
					}
				}
				
												
				if(RF_ACK_REQ)
				{
						RFRX_ACK	= 0;										// Set the RFRX_ACK bit
						//RF_CHANNEL_SCAN	= 0;									// Reset RF_CHANNEL_SCAN flag
						RFRX_ERROR++;										// Increment RFRX_ERROR counter
						RFRX_ACK_COUNT++;
				}
				
							
					//rf_rx();												// Call RF Receive function
					//RF_ERROR	= 0;								// Clear RF_ERROR flag
					//for (unsigned int i = 0; i < 12000; i++);////////////////// 1.0ms Delay
				
							
					//TRX_STATE	= TRX_OFF;									// RF Transceiver to TRX_OFF mode (XOSC=ON)
					//for (unsigned int i = 0; i < 20; i++);					// 5us Delay (1us required for transition)
							
					//for (unsigned int i = 0; i < 4000; i++);////////////////// 1.0ms Delay (Required to allow noise to settle before counting pulses)
							
					//count_int_enable(1);									// Enable Pulse Count Interrupts
					//sei();													// Enable Interrupts
											
			}
			
			
			if(RFTX_ACK_COUNT == 32)
			{
				RFTX_ACK_COUNT = 0;
			}
			
			//if (RF_ACK_REQ)
			//{
				//testrfrxerror++;////////////////////////////////// TEST CODE!!!
				//RFRX_ERROR++;										// Increment RFRX_ERROR counter
				//RFRX_ACK_COUNT++;
				//RF_ACK_REQ = 0;
				
				//if (RFRX_ERROR > RFRX_ERROR_SCAN)
				//{
				//	RF_CHANNEL_SCAN	= 1;
				//}
			//}
			
			//RFRX_ACK	= 0;										// Reset RFRX_ACK bit
			PULSE_COUNT	=	0;										// Pulse Counter Res		
			
			if(BATMON & (1 << BATMON_OK))
			{
				BATT_LOW = false;
				//led_on();
			}
			else if(BATMON & (1 << BAT_LOW))
			{
				COUNT_LOWBATT++;
				if(COUNT_LOWBATT > 15)
				{
					BATT_LOW = true;
					//COUNT_LOWBATT = 0;
					led_on();
				}
				else if(COUNT_LOWBATT > 100)
				{
					COUNT_LOWBATT = 0;
					//led_on();
				}
				
			}
			
			if (RFRX_ERROR > RFRX_ERROR_MAX)
			{
				RFRX_ERROR		= 0;								// Reset RFRX_ERROR counter
				//RF_CHANNEL_SCAN	= 0;								// Reset RF_CHANNEL_SCAN flag
				RFRX_ACK_COUNT  = 0;
				POWER_OFF		= 1;								// Turn off Probe
			}
			else
			{
				sei();												// Enable Interrupts
				TRX_STATE	= RX_ON;								// RF Transceiver to RX_ON mode (Rx Listen State)
				for (unsigned int i = 0; i < 20; i++);				// 5us Delay (1us required for transition)
				
			}
			
			TIMER_ISR	=	0;										// Clear ISR Flag
		}
		
		/////////////////////////////////
		// RF Receive is Complete ISR //
		/////////////////////////////////
		if (RF_RX_END_ISR)
		{
			cli();													// Disable Interrupts
			wdt_reset();											// Rest WDT
			RF_RX_END_ISR	=	0;									// Clear ISR Flag
			
			testrfrxcount++;////////////////////////////////////// TEST CODE!!!
			
			
			
			if(!BATT_LOW)
			{
				if (LED_BLINK_RATE < ( (LED_BLINK_RATE_MAX >> 1) & 0x7F) )
				{
					led_off();											// LED Off
				}
			}
			
			
			//RFRX_ACK	= 1;										// Set the RFRX_ACK bit
			
			//RF_CHANNEL_SCAN	= 0;									// Reset RF_CHANNEL_SCAN flag
			
			rf_rx();												// Call RF Receive function
			
			if(RF_ERROR == 0)											// No acknowledgment received earlier
			{
				RF_ACK_REQ = 0;
				RFRX_ERROR	= 0;										// Reset RFRX_ERROR counter
			}

			
			TRX_STATE	= TRX_OFF;									// RF Transceiver to TRX_OFF mode (XOSC=ON)
			for (unsigned int i = 0; i < 20; i++);					// 5us Delay (1us required for transition)
			
			//for (unsigned int i = 0; i < 4000; i++);////////////////// 1.0ms Delay (Required to allow noise to settle before counting pulses)
			
			count_int_enable(1);									// Enable Pulse Count Interrupts
			sei();													// Enable Interrupts
		}
		
		////////////////////////////////////////
		// Accelerometer Motion Interrupt ISR //
		////////////////////////////////////////
		if (POWER_ON)
		{
			cli();													// Disables interrupts
			wdt_reset();											// Reset WDT
			
			testpoweron++;//////////////////////////////////////// TEST CODE!!!
			
			accel_int1_int_enable(0);								// Disable accelerometer INT1 interrupts
			
			
			// LED TURN-ON DISPLAY
			for (unsigned int i = 0; i < 4; i++)
			{
				led_on();
		
				for (unsigned int i = 0; i < 40; i++)
				{
					for (unsigned int i = 0; i < 10000; i++);		// 2.5msec Delay
				};
		
				led_off();
				
				for (unsigned int i = 0; i < 40; i++)
				{
					for (unsigned int i = 0; i < 10000; i++);		// 2.5msec Delay
				};
			};
	
			//led_on();												// LED 'on'
			
			analog_pwr_enable(1);									// Enable power to analog circuit
			
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			
			//init_accel_spi(1);									// Enable SPI port
			init_rf(1);												// Enable RF Transceiver
			PHY_CC_CCA	=	15;										// Reset RF Channel
			set_batmon_threshold();									// Set battery monitor threshold level
			
			PROBE_CONFIG_UPDATE = 1;								//Set flag to request source settings
			
			init_dpot_spi(1);
			dpot_set(0, 0x00, AMP_GAIN);
			dpot_set(1, 0x00, COMP_L_THRESHOLD);
			dpot_set(1, 0x01, COMP_H_THRESHOLD);
			
			timer1_enable(1);										// Enable Timer 1
			count_int_enable(1);									// Enable Pulse Count interrupt
			
			POWER_ON	=	0;										// Clear the POWER_ON ISR
			POWER_OFF	=	0;										// Clear the POWER_OFF ISR
			EIFR		=	0xFF;									// Clear all external interrupt flags
			PCIFR		=	0xFF;									// Clear all pin change interrupt flags
			
			//wdt_enable(WDTO_250MS);									// Enable WDT for 250ms
			wdt_enable(WDTO_1S);									// Enable WDT for 250ms
			
			sei();													// Enables interrupts
		}
		
		if (POWER_OFF)
		{
			cli();													// Disables interrupts
			wdt_reset();											// Reset WDT
			
			testpoweroff++;/////////////////////////////////////// TEST CODE!!!
			
			PHY_CC_CCA	=	15;									// Reset RF Channel
			
			accel_int1_int_enable(1);								// Enable accelerometer INT1 interrupts
			count_int_enable(0);									// Disable Pulse Count interrupt
			timer1_enable(0);										// Disable Timer 1
			init_dpot_spi(0);										// Disable digital pot SPI port
			init_rf(0);												// Disable RF Transceiver
			//init_accel_spi(0);										// Disable Accelerometer SPI port
			
			analog_pwr_enable(0);									// Disable power to analog circuit
			
			led_off();												// LED 'off'
			
			PULSE_COUNT	=	0;										// Reset the Pulse Counter
			POWER_ON	=	0;										// Clear the POWER_ON ISR
			POWER_OFF	=	0;										// Clear the POWER_OFF ISR
			EIFR		=	0xFF;									// Clear all external interrupt flags
			PCIFR		=	0xFF;									// Clear all pin change interrupt flags
			
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			for (unsigned int i = 0; i < 40000; i++);				// 10ms Delay
			
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);					// Configure sleep mode for PWR_DOWN
			sei();													// Enables interrupts
			wdt_enable(WDTO_4S);									// Enable WDT for 4s
			sleep_mode();											// Place MCU into sleep mode
		}
	}
}